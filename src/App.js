import React from 'react';
import {connect} from 'react-redux';
import './App.css';
import * as actionTypes from './store/actions';
import Modal from './components/UI/Modal/Modal';
import GameInterface from './containers/GameInterface';
import BuyingModal from './components/UI/Modal/ModalBodies/BuyingModal'
import TransportingModal from './components/UI/Modal/ModalBodies/TransportingModal'
import EndOfWeekModal from './components/UI/Modal/ModalBodies/EndOfWeekModal'
import Gantt from './components/UI/Gantt/Gantt';


class App extends React.Component {

    constructor(props){
        super(props);

        this.transportingModal = null;
    }

    transportingModalGenerator = () => {
        this.transportingModal =
            <TransportingModal
                show={this.props.transporting}
                name ={this.props.activeTransporting}/>;
    };

    componentWillReceiveProps(nextProps) {
        if(nextProps.transporting === false) this.transportingModal = null;
    }

    render() {
        if(this.props.transporting) {
            this.transportingModalGenerator()
        } else {
            this.transportingModal = null;
        }
        return (
            <div className="App">

                <Modal show={this.props.purchasing}>
                    <BuyingModal show={this.props.purchasing}
                                 modalClosed={this.props.onPurchasingEnd}/>
                </Modal>
                <Modal show={this.props.ganttShow} classes = "Modal GanttModal">
                    <Gantt buttonHandler = {this.props.onGanttHide}/>
                </Modal>
                <Modal show={this.props.transporting}>
                    {this.transportingModal}
                </Modal>
                <Modal
                    show={this.props.endOfWeek}
                    classes = "Modal EndOfWeekModal"
                >
                    <EndOfWeekModal
                    show = {this.props.endOfWeek}/>
                </Modal>

                <GameInterface/>

            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        purchasing: state.showModal.buyingModal,
        ganttShow: state.showModal.ganttModal,
        transporting: state.showModal.transportingModal,
        endOfWeek: state.showModal.endOfWeekModal,
        gameInfo: state.gameInfo,
        activePurchasing: state.activePurchasing,
        activeTransporting: state.activeTransporting,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onPurchasingEnd: () => dispatch({type: actionTypes.HIDE_MODAL, modalToClose: "buyingModal"}),
        onGanttHide: () => dispatch({type: actionTypes.HIDE_MODAL, modalToClose: "ganttModal"})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

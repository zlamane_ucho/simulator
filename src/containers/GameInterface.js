import React, {Component} from 'react';
import {connect} from 'react-redux';

import '../App.css';

import Stations from '../components/Stations/Stations';
import UsersControls from "../components/UsersControls/UsersControls"
import GameInfo from '../components/GameInfo/GameInfo';

class GameInterface extends Component {



    render() {
        return (
            <div className="App">
                <div className="container">
                    <Stations/>
                    <div className="ui-wrapper">
                        <UsersControls />
                        <GameInfo />
                    </div>
                </div>
            </div>
        );
    }


}

const mapStateToProps = state => {
    return {
        showModal: state.showModal,
        stations: state.stations
    };
};


export default connect(mapStateToProps)(GameInterface);
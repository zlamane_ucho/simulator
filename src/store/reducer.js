import * as actionTypes from './actions';

const initState = {
    //ganttInfo name of arrays must be the same as names of stations!
    //gameInfo cost info buying / selling must be the same sam names of products on stations!
    stations: {

        A: {
            position: {
                row: 1,
                col: 1
            },
            resource: "A",
            productName: "P1",
            inputStations: [],
            outputStations: ["C", "D"],
            status: {
                isSelling: false,
                isBuying: true,
                isPreparing: false,
                isWorking: false,
                timeSetting: {
                    timeToMakePart: 5,
                    timeToPrepare: 5,
                    workingTime: 0,
                    preparingTime: 0
                },
                piecesToDo: {
                    //names of properties are the same as names of input stations or BOUGHT if you can buy on this station
                    bought: [0,1]
                },
                piecesDone: 0
            }
        },
        B: {
            position: {
                row: 1,
                col: 3
            },
            resource: "A",
            productName: "P2",
            inputStations: [],
            outputStations: ["D", "E"],
            status: {
                isSelling: false,
                isBuying: true,
                isPreparing: false,
                isWorking: false,
                timeSetting: {
                    timeToMakePart: 5,
                    timeToPrepare: 5,
                    workingTime: 0,
                    preparingTime: 0
                },
                piecesToDo: {
                    bought: [0,1]
                },
                piecesDone: 0
            }
        },
        C: {
            position: {
                row: 3,
                col: 1
            },
            resource: "B",
            productName: "P4",
            inputStations: ["A"],
            outputStations: ["F"],
            status: {
                isSelling: true,
                isBuying: false,
                isPreparing: false,
                isWorking: false,
                timeSetting: {
                    timeToMakePart: 5,
                    timeToPrepare: 5,
                    workingTime: 0,
                    preparingTime: 0
                },
                piecesToDo: {
                    P1: [0,1]
                },
                piecesDone: 0
            }
        },
        D: {
            position: {
                row: 2,
                col: 2
            },
            resource: "C",
            productName: "P3",
            inputStations: ["A", "B"],
            outputStations: ["F"],
            status: {
                isSelling: false,
                isBuying: false,
                isPreparing: false,
                isWorking: false,
                timeSetting: {
                    timeToMakePart: 5,
                    timeToPrepare: 5,
                    workingTime: 0,
                    preparingTime: 0
                },
                piecesToDo: {
                    P1: [0,1],
                    P2: [0,1]
                },
                piecesDone: 0
            }
        },
        E: {
            position: {
                row: 3,
                col: 3
            },
            resource: "B",
            productName: "P4",
            inputStations: ["B"],
            outputStations: ["F"],
            status: {
                isSelling: true,
                isBuying: false,
                isPreparing: false,
                isWorking: false,
                timeSetting: {
                    timeToMakePart: 5,
                    timeToPrepare: 5,
                    workingTime: 0,
                    preparingTime: 0
                },
                piecesToDo: {
                    P2: [0,1]
                },
                piecesDone: 0
            }
        },
        F: {
            position: {
                row: 4,
                col: 2
            },
            resource: "C",
            productName: "P",
            inputStations: ["C", "D", "E"],
            outputStations: [],
            status: {
                isSelling: true,
                isBuying: false,
                isPreparing: false,
                isWorking: false,
                timeSetting: {
                    timeToMakePart: 5,
                    timeToPrepare: 5,
                    workingTime: 0,
                    preparingTime: 0
                },
                piecesToDo: {
                    P3: [0,1],
                    P4: [0,2]
                },
                piecesDone: 0
            }
        }
    },
    gameInfo: {
        week: 1,
        day: {
            number: 1,
            name: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday','']
        },
        time: {
            hour: 0,
            minute: 0
        },
        money: 1500,
        costInfo: {
            buying: {
                P1: 10,
                P2: 10
            },
            selling: {
                P1: 40,
                P2: 30,
                P: 60
            },
            operationalExpenses: 2500
        },
        endOfWeek : {
            startingMoney: null,
            bought: {},
            sold: {}
        },
        isStarted: false,
        gameSpeed: 1,
        gameGrid: {
            rows: 4,
            cols: 3
        }
    },
    ganttInfo: {
        A: [],
        B: [],
        C: [],
        D: [],
        E: [],
        F: []
    },
    showModal: {
        buyingModal: false,
        transportingModal: false,
        ganttModal: false,
        endOfWeekModal: false
    },
    activePurchasing: '',
    activeTransporting: ''
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case actionTypes.SHOW_MODAL:
            if(action.purchasingStation){
                return {
                    ...state,
                    showModal: {
                        ...state.showModal,
                        [action.modalToShow]: true
                    },
                    activePurchasing: action.purchasingStation
                };
            } else {
                if(action.transportingStation){
                    return {
                        ...state,
                        showModal: {
                            ...state.showModal,
                            [action.modalToShow]: true
                        },
                        activeTransporting: action.transportingStation
                    };
                } else {
                    return {
                        ...state,
                        showModal: {
                            ...state.showModal,
                            [action.modalToShow]: true
                        }
                    };
                }
            }

        case actionTypes.START_GAME:
            let objToReturn = {
                ...state,
                gameInfo: {
                    ...state.gameInfo,
                    isStarted: !state.gameInfo.isStarted
                }
            };
            if(state.gameInfo.endOfWeek.startingMoney === null) {
                objToReturn.gameInfo.endOfWeek.startingMoney = state.gameInfo.money;
            }
            return objToReturn;

        case actionTypes.HIDE_MODAL:
            return {
                ...state,
                showModal: {
                    ...state.showModal,
                    [action.modalToClose]: false
                }
            };
        case actionTypes.ADD_PRODUCT_TO_STATION:
            let newBoughtArr = [...state.stations[state.activePurchasing].status.piecesToDo.bought];
            newBoughtArr[0] += action.amount;
            if (!isNaN(action.amount) && action.amount >= 0) {
                let buying = state.stations[state.activePurchasing].productName;
                objToReturn = {
                    ...state,
                    stations: {
                        ...state.stations,
                        [state.activePurchasing]: {
                            ...state.stations[state.activePurchasing],
                            status: {
                                ...state.stations[state.activePurchasing].status,
                                piecesToDo: {
                                    ...state.stations[state.activePurchasing].status.piecesToDo,
                                    bought: newBoughtArr
                                }
                            }
                        }
                    },
                    gameInfo: {
                        ...state.gameInfo,
                        money:
                        state.gameInfo.money -
                        state.gameInfo.costInfo.buying[buying] * action.amount
                    },
                    showModal: {
                        ...state.showModal,
                        buyingModal: false
                    }
                };
                let endOfWeek = {};
                if(state.gameInfo.endOfWeek.bought.hasOwnProperty(buying)){
                    endOfWeek = {
                        ...state.gameInfo.endOfWeek,
                        bought: {
                            ...state.gameInfo.endOfWeek.bought,
                            [buying]: state.gameInfo.endOfWeek.bought[buying] + action.amount
                        }
                    }
                } else {
                    endOfWeek = {
                        ...state.gameInfo.endOfWeek,
                        bought: {
                            ...state.gameInfo.endOfWeek.bought,
                            [buying]: action.amount
                        }
                    }
                }
                objToReturn.gameInfo.endOfWeek = endOfWeek;
                if(state.gameInfo.endOfWeek.startingMoney === null) {
                    objToReturn.gameInfo.endOfWeek.startingMoney = state.gameInfo.money;
                }
                return objToReturn;
            } else return {...state};
        case actionTypes.START_PREPARING:
            let stationType = state.stations[action.stationName].resource;

            // Which stations have to be turned off
            let stationsWithType = Object.keys(state.stations).filter(item => {
                return (state.stations[item].resource === stationType && item !== action.stationName);
            });

            //Prepare object
            objToReturn = {
                ...state,
                stations: {
                    ...state.stations,
                    [action.stationName]: {
                        ...state.stations[action.stationName],
                        status: {
                            ...state.stations[action.stationName].status,
                            isPreparing: true
                        }
                    }
                }
            };

            //Turn off stations
            for(let i = 0; i < stationsWithType.length; i++){
              objToReturn.stations[stationsWithType[i]] = {
                  ...state.stations[stationsWithType[i]],
                  status: {
                      ...state.stations[stationsWithType[i]].status,
                      isPreparing: false,
                      isWorking: false,
                      timeSetting: {
                          ...state.stations[stationsWithType[i]].status.timeSetting,
                          workingTime: 0,
                          preparingTime: 0
                      }
                  }
              };
            }

            return objToReturn;

        case actionTypes.ACTIVATE_STATION:
            return {
                ...state,
                stations: {
                    ...state.stations,
                    [action.stationName]: {
                        ...state.stations[action.stationName],
                        status: {
                            ...state.stations[action.stationName].status,
                            isPreparing: false,
                            isWorking: true
                        }
                    }
                }
            };
        case actionTypes.TIME_TICK:
            return {
                ...state,
                stations: {
                    ...state.stations,
                    [action.stationName]: {
                        ...state.stations[action.stationName],
                        status: {
                            ...state.stations[action.stationName].status,
                            timeSetting: {
                                ...state.stations[action.stationName].status.timeSetting,
                                workingTime: state.stations[action.stationName].status.timeSetting.workingTime + 1
                            }
                        }
                    }
                }
            };
        case actionTypes.PREPARE_TICK:
            return {
                ...state,
                stations: {
                    ...state.stations,
                    [action.stationName]: {
                        ...state.stations[action.stationName],
                        status: {
                            ...state.stations[action.stationName].status,
                            timeSetting: {
                                ...state.stations[action.stationName].status.timeSetting,
                                preparingTime: state.stations[action.stationName].status.timeSetting.preparingTime + 1
                            }
                        }
                    }
                }
            };
        case actionTypes.PIECE_DONE:
            objToReturn = {
                ...state,
                stations: {
                    ...state.stations,
                    [action.stationName]: {
                        ...state.stations[action.stationName],
                        status: {
                            ...state.stations[action.stationName].status,
                            piecesDone: state.stations[action.stationName].status.piecesDone + 1,
                            timeSetting: {
                                ...state.stations[action.stationName].status.timeSetting,
                                workingTime: 0.0
                            }
                        }

                    }
                }
            };
            let piecesToDecrease = Object.keys(state.stations[action.stationName].status.piecesToDo);
            let doPieces = {
                ...state.stations[action.stationName].status.piecesToDo
            };

            for(let i = 0; i < piecesToDecrease.length; i++) {
                let actualPiecesToDo = state.stations[action.stationName].status.piecesToDo[piecesToDecrease[i]];
                let newArr = [...actualPiecesToDo];
                newArr[0] -= [actualPiecesToDo[1]];
                doPieces[piecesToDecrease[i]] = newArr;
            }

            objToReturn.stations[action.stationName].status.piecesToDo = doPieces;

            return objToReturn;
        // case actionTypes.MAIN_PIECE_DONE:
        //     return {
        //         ...state,
        //         stations: {
        //             ...state.stations,
        //             [action.stationName]: {
        //                 ...state.stations[action.stationName],
        //                 piecesToDoA: state.stations[action.stationName].piecesToDoA - 1,
        //                 piecesToDoB: state.stations[action.stationName].piecesToDoB - 1,
        //                 piecesDone: state.stations[action.stationName].piecesDone + 1,
        //                 workingTime: 0.0
        //             }
        //         },
        //         gameInfo: {
        //             ...state.gameInfo,
        //             money:
        //             state.gameInfo.money + state.gameInfo.costInfo.mainProduct
        //         }
        //     };
        case actionTypes.TRANSPORT_PIECES:
            let transportingProduct = state.stations[action.stationName].productName;

            let amount = 0;
            if(action.amount > state.stations[action.stationName].status.piecesDone) {
                amount = state.stations[action.stationName].status.piecesDone;
            } else if(action.amount < 0) {
                amount = 0;
            } else {
                amount = action.amount;
            }

            let newPiecesToDoArr = [...state.stations[action.transportTo].status.piecesToDo[transportingProduct]];
            newPiecesToDoArr[0] += amount;

            return {
                ...state,
                stations: {
                    ...state.stations,
                    [action.stationName]: {
                        ...state.stations[action.stationName],
                        status : {
                            ...state.stations[action.stationName].status,
                            piecesDone: state.stations[action.stationName].status.piecesDone - amount
                        }
                    },
                    [action.transportTo]: {
                        ...state.stations[action.transportTo],
                        status : {
                            ...state.stations[action.transportTo].status,
                            piecesToDo: {
                                ...state.stations[action.transportTo].status.piecesToDo,
                                [transportingProduct]: newPiecesToDoArr
                            }
                        }
                    }
                }
            };
        case actionTypes.SELL_PIECES:
            let selling = state.stations[action.stationName].productName;

            if(state.stations[action.stationName].status.piecesDone) {
                objToReturn = {
                    ...state,
                    stations: {
                        ...state.stations,
                        [action.stationName]: {
                            ...state.stations[action.stationName],
                            status: {
                                ...state.stations[action.stationName].status,
                                piecesDone: 0
                            }
                        },
                    },
                    gameInfo: {
                        ...state.gameInfo,
                        money:
                        state.gameInfo.money +
                        state.stations[action.stationName].status.piecesDone * state.gameInfo.costInfo.selling[selling]
                    }
                };

                let endOfWeek = {};
                console.log(selling, state.gameInfo.endOfWeek.sold.hasOwnProperty(selling));
                if(state.gameInfo.endOfWeek.sold.hasOwnProperty(selling)){
                    console.log(true);
                    endOfWeek = {
                        ...state.gameInfo.endOfWeek,
                        sold: {
                            ...state.gameInfo.endOfWeek.sold,
                            [selling]: state.gameInfo.endOfWeek.sold[selling] + state.stations[action.stationName].status.piecesDone
                        }
                    }
                } else {
                    endOfWeek = {
                        ...state.gameInfo.endOfWeek,
                        sold: {
                            ...state.gameInfo.endOfWeek.sold,
                            [selling]: state.stations[action.stationName].status.piecesDone
                        }
                    };
                    console.log(endOfWeek);
                }
                objToReturn.gameInfo.endOfWeek = endOfWeek;
                return objToReturn;
            } else return {...state};
        case actionTypes.TIME_STAMP:
            let tempArr = [...state.ganttInfo[action.stationName]];
            tempArr.push(action.timeStamp);
            return {
                ...state,
                ganttInfo: {
                    ...state.ganttInfo,
                    [action.stationName]: tempArr
                }
            };
        case actionTypes.CHANGE_MINUTE:
            return {
                ...state,
                gameInfo: {
                    ...state.gameInfo,
                    time: {
                        ...state.gameInfo.time,
                        minute: state.gameInfo.time.minute + 1
                    }
                }
            };
        case actionTypes.CHANGE_HOUR:
            return {
                ...state,
                gameInfo: {
                    ...state.gameInfo,
                    time: {
                        ...state.gameInfo.time,
                        minute: 0,
                        hour: state.gameInfo.time.hour + 1
                    }
                }
            };
        case actionTypes.CHANGE_DAY:
            return {
                ...state,
                gameInfo: {
                    ...state.gameInfo,
                    time: {
                        ...state.gameInfo.time,
                        hour: 0
                    },
                    day: {
                        ...state.gameInfo.day,
                        number: state.gameInfo.day.number + 1
                    }
                }
            };
        case actionTypes.CHANGE_WEEK:
            return {
                ...state,
                gameInfo: {
                    ...state.gameInfo,
                    day: {
                        ...state.gameInfo.day,
                        number: 1
                    },
                    week: state.gameInfo.week + 1,
                    money: state.gameInfo.money - state.gameInfo.costInfo.operationalExpenses,
                    isStarted: false
                },
                showModal: {
                    ...state.showModal,
                    endOfWeekModal: true
                }
            };
        case actionTypes.SPEED_UP:
            let gameSpeed = state.gameInfo.gameSpeed;
            (gameSpeed + 1) > 10 ? gameSpeed = 10 : gameSpeed += 1;
            return {
                ...state,
                gameInfo: {
                    ...state.gameInfo,
                    gameSpeed: gameSpeed
                }
            };
        case actionTypes.SPEED_DOWN:
            gameSpeed = state.gameInfo.gameSpeed;
            (gameSpeed - 1) < 1 ? gameSpeed = 1 : gameSpeed -= 1;
            return {
                ...state,
                gameInfo: {
                    ...state.gameInfo,
                    gameSpeed: gameSpeed
                }
            };
        case actionTypes.GAME_STATUS:
            if(state.gameInfo.money < 0) {
                return {
                    ...initState
                }
            } else {
                return {
                    ...state,
                    gameInfo: {
                        ...state.gameInfo,
                        isStarted: true
                    }
                }
            }
        default:
            return {...state};
    }
};

export default reducer;

import React, {Component} from 'react';
import {connect} from 'react-redux'
import ControlButton from '../../../components/UI/Buttons/ControlButton';
import * as actionTypes from '../../../store/actions';


class BuyingButtons extends Component{

    buttonsToRender = ()=> {

        return Object.keys(this.props.stations).filter(item => {
            return this.props.stations[item].status.isSelling
        }).map(item => {
            // text for long buttons
            // let buttonText = `Sell product ${this.props.stations[item].productName} on station ${item}`;

            // text for short buttons
            let buttonText = `Sell ${this.props.stations[item].productName} on ${item}`;
            return <ControlButton
                key = {`${Math.random().toString(36).substr(2, 9)}`}
                classes="btn btn-primary btn-block SellingButton"
                buttonHandler={() => this.props.onSelling(item)}
                text={buttonText}/>
        })

    };


    render() {

        return <div className="btns">{this.buttonsToRender()}</div>;

    }

}

const mapStateToProps = state => {
    return {
        stations: state.stations
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onSelling: (sellingStation) => dispatch({type: actionTypes.SELL_PIECES, stationName: sellingStation})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(BuyingButtons);
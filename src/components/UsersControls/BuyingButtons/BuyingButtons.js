import React, {Component} from 'react';
import {connect} from 'react-redux'
import ControlButton from '../../../components/UI/Buttons/ControlButton';
import * as actionTypes from '../../../store/actions';


class BuyingButtons extends Component{

    buttonsToRender = ()=> {

        return Object.keys(this.props.stations).filter(item => {
            return this.props.stations[item].status.isBuying
        }).map(item => {
            // let buttonText = `Buy product ${this.props.stations[item].productName} on station ${item}`;
            let buttonText = `Buy ${this.props.stations[item].productName} on ${item}`;
            return <ControlButton
                    key = {`${Math.random().toString(36).substr(2, 9)}`}
                    classes="btn btn-primary btn-block"
                    buttonHandler={() => this.props.onPurchasing(item)}
                    text={buttonText}/>
        })

    };


    render() {

        return <div className="btns">{this.buttonsToRender()}</div>;

    }

}

const mapStateToProps = state => {
    return {
        stations: state.stations
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onPurchasing: (purchasingStation) => dispatch({type: actionTypes.SHOW_MODAL, purchasingStation: purchasingStation, modalToShow: "buyingModal"})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(BuyingButtons);
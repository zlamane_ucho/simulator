import React from 'react';
import {connect} from 'react-redux';
import './UsersControls.css'
import ControlButton from '../../components/UI/Buttons/ControlButton';
import IOButton from '../../components/UI/Buttons/IOButton';
import * as actionTypes from '../../store/actions';
import BuyingButtons from "./BuyingButtons/BuyingButtons"
import SellingButtons from "./SellingButtons/SellingButtons"

const usersControls = props => {
    return(
        <div className="controls">
            <IOButton
                isStarted = {props.gameInfo.isStarted}
                onGameStart = {props.onGameStart}/>
            <div className="btns-wrapper">
                <BuyingButtons/>
                <SellingButtons/>
            </div>

            <ControlButton
                classes = "btn btn-dark btn-mrg"
                buttonHandler = {props.onGanttShow}
                text = "Show Gantt"
            /><br/>

            <ControlButton classes="btn btn-dark btn-round"
                           buttonHandler={props.onSpeedDown}
                           text="-"/>
            <span className="game-speed"> {props.gameInfo.gameSpeed} </span>
            <ControlButton classes="btn btn-dark btn-round"
                           buttonHandler={props.onSpeedUp}
                           text="+"/>
        </div>
    );
};

const mapStateToProps = state => {
    return {
        gameInfo: state.gameInfo
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onGameStart: () => dispatch({type: actionTypes.START_GAME}),
        onSpeedUp: () => dispatch({type: actionTypes.SPEED_UP}),
        onSpeedDown: () => dispatch({type: actionTypes.SPEED_DOWN}),
        onGanttShow: () => dispatch({type: actionTypes.SHOW_MODAL, modalToShow: "ganttModal"}),
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(usersControls);

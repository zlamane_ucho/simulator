import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actionTypes from '../../store/actions';


class GameInfo extends Component {

    constructor(props) {
        super(props);
        this.tick = this.tick.bind(this);
    }


    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.gameInfo.gameSpeed !== this.props.gameInfo.gameSpeed) {
            clearInterval(this.timerID);
            this.timerID = setInterval(
                () => this.tick(),
                1000 / nextProps.gameInfo.gameSpeed
            );
        }
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        //add 1 minute on tick
        if(this.props.gameInfo.isStarted)this.props.onMinuteChange();

        if(this.props.gameInfo.time.minute === 60) this.props.onHourDone();
        if(this.props.gameInfo.time.hour === 8) this.props.onDayDone();
        if(this.props.gameInfo.day.number === 6) this.props.onWeekDone();
    }

    render() {
        let stateMinutes = this.props.gameInfo.time.minute;
        let minutesToShow = stateMinutes >= 10 ? stateMinutes : ("0" + stateMinutes);
        return (
            <div className="game-info">
                Info
                <ul>
                    <li>Week: {this.props.gameInfo.week} </li>
                    <li>Day: {this.props.gameInfo.day.name[this.props.gameInfo.day.number - 1]} </li>
                    <li>Time: {this.props.gameInfo.time.hour}:{minutesToShow}</li>
                    <li>Money: {this.props.gameInfo.money}</li>
                </ul>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        gameInfo: state.gameInfo
    }
};

const mapDispatchToProps = dispatch => {
  return {
      onMinuteChange: () => dispatch({type: actionTypes.CHANGE_MINUTE}),
      onHourDone: () => dispatch({type: actionTypes.CHANGE_HOUR}),
      onDayDone: () => dispatch({type: actionTypes.CHANGE_DAY}),
      onWeekDone: () => dispatch({type: actionTypes.CHANGE_WEEK})
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(GameInfo);

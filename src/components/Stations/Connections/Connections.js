import React, {Component} from 'react';

import {connect} from 'react-redux';

class Connections extends Component{

    prepareConnections = () => {

        let stations = this.props.stations;

        let connToRend = Object.keys(stations).map(el => {

            let linesToRend = [];

            let elPos = {
                x: stations[el].position.col,
                y: stations[el].position.row
            };


            stations[el].outputStations.forEach(out => {

                let outPos = {
                    x: stations[out].position.col,
                    y: stations[out].position.row
                };


                let line = this.makeLines(this.relativePos(elPos, outPos), this.distances(elPos, outPos), elPos);

                linesToRend.push(line);

                let arrow = this.makeArrow(this.relativePos(elPos, outPos),this.distances(elPos, outPos), outPos);

                linesToRend.push(arrow);
            });

            return linesToRend;

        });

        let rend = [];

        connToRend.forEach(el => {
            rend = [...rend, ...el];
        });

        return rend;

    };

    makeArrow(relative, distance, pos) {
        let stHeight = 25;
        let stWidth = 100/3;

        let styles = {
            position: 'absolute',
            zIndex: '10',
            width: '0',
            height: '0',
            borderStyle: 'solid',
            borderWidth: '8px'

        };
        switch (relative) {
            case "ver":
                styles.top = (pos.y-1) * stHeight + "vh";
                styles.left= ((pos.x-1) * stWidth + 0.5*stWidth) + "%";
                styles.transform = "translateX(-50%)";
                styles.borderColor = 'red transparent transparent transparent';
                return <div key = {Math.random().toFixed(6)} style = {styles}></div>;
            case "hor":
            case "ask":
                styles.top = (pos.y - 1 + 0.5) * stHeight + "vh";
                styles.transform = "translateY(-50%)";
                if (distance.x > 0) {
                    styles.left= ((pos.x-1) * stWidth) + "%";
                    styles.borderColor = 'transparent transparent transparent red';
                } else {
                    styles.left= ((pos.x) * stWidth) + "%";
                    styles.borderColor = 'transparent red transparent transparent';
                    styles.transform = 'translate(-100%,-50%)'
                }

                return <div key = {Math.random().toFixed(6)} style = {styles}></div>;
            default: return null;
        }
    }

    makeLines = (relative, distances, pos) => {


        let stHeight = 25;
        let stWidth = 100/3;
        let lines = [];

        let vertIndent = 0.6;
        let horIndent = 0.52;
        let horInd = 0.8;

        let height;
        let width;
        let top;
        let left;
        let transform;


        switch(relative) {
            case "ver":
                top = (stHeight*(pos.y - 1) + stHeight - vertIndent) + "vh";
                left = (stWidth*(pos.x - 1) + stWidth/2) + "%";
                width = "5px";
                height = (stHeight*(Math.abs(distances.y) - 1) + vertIndent*2) + "vh";
                transform = 'translate(-50%)';

                lines.push(this.makeLine(top, left, width, height, transform));
                break;
            case "hor":
                top = (stHeight*(pos.y - 1) + stHeight/2) + "vh";
                left = (stWidth*(pos.x - 1) + stWidth - horInd) + "%";
                width = (stWidth*(Math.abs(distances.x) - 1) + horInd*2) + "%";
                height = "5px";
                transform = 'translate(0,-50%)';

                lines.push(this.makeLine(top, left, width, height, transform));

                break;
            case "ask":
                //vertical line
                top = (stHeight*(pos.y - 1) + stHeight - vertIndent) + "vh";
                left = (stWidth*(pos.x - 1) + stWidth/2) + "%";
                width = "5px";
                height = (stHeight*(Math.abs(distances.y) - 1) + vertIndent + stHeight/2) + "vh";
                transform = 'translate(-50%)';
                lines.push(this.makeLine(top, left, width, height, transform));


                //horizontal line
                //top = start of ver line + height of ver line
                top = (stHeight*(pos.y - 1) + stHeight - vertIndent) + (stHeight*(Math.abs(distances.y) - 1) + vertIndent + stHeight/2) + "vh";
                width = (stWidth*(Math.abs(distances.x) - 1) + stWidth*horIndent) + "%";
                height = "5px";
                transform = 'translate(0,-50%)';
               if(distances.x > 0) {
                   left = (stWidth*(pos.x - 1) + stWidth/2) + "%";
               }
               else {
                   // left = center of parent station - width
                   left = (stWidth*(pos.x - 1) + stWidth/2) - (stWidth*(Math.abs(distances.x) - 1) + stWidth*horIndent) + "%";
               }
                lines.push(this.makeLine(top, left, width, height, transform));

                break;
            default:
                break;
        }

        return lines;

    };

    makeLine = (top, left, width, height, transform) => {
        let styles = {
            backgroundColor: '#ff0000',
            position: 'absolute',
            zIndex: '10',
        };

        styles.top = top;
        styles.left = left;
        styles.width = width;
        styles.height = height;
        styles.transform = transform;

        return <div key = {Math.random().toFixed(6)} style = {styles}></div>

    };


    distances = (elPos, outPos) => {
        return {
            x: (outPos.x - elPos.x),
            y: (outPos.y - elPos.y)
        }
    };

    relativePos = (elPos, outPos) => {

        if(elPos.y === outPos.y) {
            return "hor";
        }
        else if(elPos.x === outPos.x) {
            return "ver"
        }
        else {
            return "ask"
        }

    };



    render() {
        return this.prepareConnections();
    }

}


const mapStateToProps = state => {
    return {
        stations: state.stations,
    }
};

export default connect(mapStateToProps)(Connections);
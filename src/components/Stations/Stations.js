import React, {Component} from 'react';
import Station from './Station/Station';
import Connections from './Connections/Connections'
import Aux from '../../hoc/Auxiliary';
import './Stations.css'

import {connect} from 'react-redux';


class Stations extends Component{


    sortByValues(arr) {
        for(let i = 0; i < arr.length - 1; i++){
            for(let j = 0; j < arr.length - 1 - i; j++){
                if((arr[j].value - arr[j+1].value)>0){
                    let temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
    }

    stationsToRender(arr){
        let statToRend = [];
        let i = 0;
        let key = '_';
        // Grid rows * cols
        for(let j = 1; j <= this.props.grid.cols * this.props.grid.rows; j++){
            if(i < arr.length){
                if(arr[i].value === j){
                    statToRend.push(arr[i].name);
                    i++;
                } else {
                    key += Math.random().toString(36).substr(2, 9);
                    statToRend.push(key);
                    key = '_';
                }
            } else {
                key += Math.random().toString(36).substr(2, 9);
                statToRend.push(key);
                key = '_';
            }

        }
        return statToRend;
    }

    render() {
        let objList = Object.keys(this.props.stations).filter(
            item => this.props.stations[item].hasOwnProperty("position")
        ).map(
            item => {
                return {
                    name: item,
                    value: (this.props.stations[item].position.row - 1) * 3 + this.props.stations[item].position.col
                }
            }
        );
        this.sortByValues(objList);
        let statToRend = this.stationsToRender(objList).map(
            item => {
                return <Station key={item} name = {item}/>
            }
        );

        return (
            <Aux>
                <div className="stations-wrapper">
                    <Connections/>
                    {statToRend}
                </div>
            </Aux>
        )
    }

}

const mapStateToProps = state => {
    return {
        stations: state.stations,
        grid: state.gameInfo.gameGrid
    }
};

export default connect(mapStateToProps)(Stations);
import React, {Component} from 'react'
import * as actionTypes from '../../../../store/actions';
import {connect} from 'react-redux';
import Light from "../../../UI/WorkIndicator/WorkIndicator";


class AssemblyStation extends Component {

    constructor(props) {
        super(props);
        this.tick = this.tick.bind(this);
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000/this.props.gameInfo.gameSpeed
        );
    }
    componentDidUpdate(nextProps) {
        if(nextProps.gameInfo.gameSpeed !== this.props.gameInfo.gameSpeed) {
            this.timerID = setInterval(
                () => this.tick(),
                1000 / this.props.gameInfo.gameSpeed
            );
        }
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        if (!this.props.station[this.props.name].active &&
            this.props.station[this.props.name].isStationPreparing &&
            this.props.gameInfo.isStarted
        ) {
            this.props.onPrepareTick(this.props.name);
            if (this.props.station[this.props.name].preparingTime >= this.props.station[this.props.name].timeForPrepare) {
                this.props.onActivateStation(this.props.name);
            }
        }

        if (
            this.props.station[this.props.name].active &&
            this.props.station[this.props.name].piecesToDoA &&
            this.props.station[this.props.name].piecesToDoB &&
            this.props.gameInfo.isStarted
        ) {
            this.props.onTimeTick(this.props.name);

            if (
                this.props.station[this.props.name].workingTime >= this.props.station[this.props.name].timeForPart
            ) this.props.onMainPieceDone(this.props.name);
        }
    }

    render() {
        let lightIndicator = <Light
            buttonHandler={() => this.props.onPrepareStart(this.props.name)}
            activateYellow = {this.props.station[this.props.name].isStationPreparing && !this.props.gameInfo.isStarted}
            activateGreen = {this.props.station[this.props.name].active && !(this.props.station[this.props.name].piecesToDo) }
            activateGreenFlash = {this.props.station[this.props.name].active && (this.props.station[this.props.name].piecesToDo) }
            activateYellowFlash = {this.props.station[this.props.name].isStationPreparing && this.props.gameInfo.isStarted}
        />;

        return (
            <div className="main-station" id={this.props.name}>
                <span className="todo">
                    A(pcs): {this.props.station[this.props.name].piecesToDoA}<br/>
                    B(pcs): {this.props.station[this.props.name].piecesToDoB}
                </span>
                {/*<div> {this.props.name} </div>*/}
                {lightIndicator}
                <span className="done">
                    DONE: {this.props.station[this.props.name].piecesDone}
                </span>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        station: state.stations,
        gameInfo: state.gameInfo
    }
};

const mapDispatachToProps = dispatch => {
    return {
        onActivateStation: (stationName) => dispatch({type: actionTypes.ACTIVATE_STATION, stationName: stationName}),
        onPrepareStart: (stationName) => dispatch({type: actionTypes.START_PREPARING, stationName: stationName}),
        onTimeTick: (stationName) => dispatch({type: actionTypes.TIME_TICK, stationName: stationName}),
        onPrepareTick: (stationName) => dispatch({type: actionTypes.PREPARE_TICK, stationName: stationName}),
        onMainPieceDone: (stationName) => dispatch({type: actionTypes.MAIN_PIECE_DONE, stationName: stationName}),
    }
};

export default connect(mapStateToProps, mapDispatachToProps)(AssemblyStation);
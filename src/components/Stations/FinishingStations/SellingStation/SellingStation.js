import React from 'react'
import {connect} from 'react-redux';
import Button from '../../../UI/Buttons/ControlButton';
import * as actionTypes from '../../../../store/actions';


const sellingStation = (props) => {
    return (
        <div className="sell-station">
            <p>{props.name}</p>
            <Button
                buttonHandler={() => props.onPiecesSell(props.name)}
                classes="btn btn-warning btn-sm"
                text="SELL PART"/>
            <div className="sold" align="center">
                SOLD PIECES OF {props.type}<br></br>
                {props.station[props.name].piecesSold}
            </div>
        </div>
    )
};

const mapStateToProps = state => {
    return {
        station: state.stations
    }
};

const mapDispatchToProps = dispatch => {
  return {
      onPiecesSell: (stationName) => dispatch({type: actionTypes.SELL_PIECES, stationName: stationName})
  }
};


export default connect(mapStateToProps, mapDispatchToProps)(sellingStation);
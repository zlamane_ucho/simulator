import React from 'react';
import SellingStation from './SellingStation/SellingStation';
import Aux from '../../../hoc/Auxiliary';
import AssemblyStation from './AssemblyStation/AssemblyStation'

const finishingStations = (props) => {
    return (
        <Aux>
            <SellingStation name="SellA" type="A"/>
            <AssemblyStation name = "E" />
            <SellingStation name="SellB" type="B"/>
        </Aux>
    );
};

export default finishingStations;
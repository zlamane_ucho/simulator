import React, {Component} from 'react'
import './Station.css'
import * as actionTypes from '../../../store/actions';
import {connect} from 'react-redux';
import Button from "../../UI/Buttons/ControlButton";
import Light from "../../UI/WorkIndicator/WorkIndicator";
import Aux from "../../../hoc/Auxiliary";

class Station extends Component {

    constructor(props) {
        super(props);
        this.tick = this.tick.bind(this);

        this.classes = "working-station";
        this.transportButtonHandler = this.transportButtonHandler.bind(this);
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000 / this.props.gameInfo.gameSpeed
        );
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.gameInfo.gameSpeed !== this.props.gameInfo.gameSpeed) {
            clearInterval(this.timerID);

            this.timerID = setInterval(
                () => this.tick(),
                1000 / nextProps.gameInfo.gameSpeed
            );
        }

        if(this.props.station.hasOwnProperty(this.props.name)){
            if(nextProps.station[this.props.name].status.isWorking || nextProps.station[this.props.name].status.isPreparing) {
                this.classes = `working-station active${this.props.station[this.props.name].resource}`
            } else this.classes = `working-station ${this.props.station[this.props.name].resource}`;

            this.makeTimeStamp(nextProps);
        }

    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        if(this.props.station.hasOwnProperty(this.props.name)){
            //Preparing station
            if (!this.props.station[this.props.name].status.isWorking &&
                this.props.station[this.props.name].status.isPreparing &&
                this.props.gameInfo.isStarted
            ) {
                this.props.onPrepareTick(this.props.name);
                if (this.props.station[this.props.name].status.timeSetting.preparingTime >= this.props.station[this.props.name].status.timeSetting.timeToPrepare) {
                    this.props.onActivateStation(this.props.name);
                }
            }
            //Making pieces
            if (
                this.props.station[this.props.name].status.isWorking &&
                this.checkPiecesToDo() &&
                this.props.gameInfo.isStarted
            ) {
                this.props.onTimeTick(this.props.name);
                if (
                    this.props.station[this.props.name].status.timeSetting.workingTime >= this.props.station[this.props.name].status.timeSetting.timeToMakePart
                ) this.props.onPieceDone(this.props.name);
            }
        }
    }

    transportButtonHandler = () => {
        if(this.props.station[this.props.name].outputStations.length > 1
        || this.props.station[this.props.name].status.isSelling){
            this.props.onTransportModalShow(this.props.name);
        } else {
            this.props.onPiecesTransport(
                this.props.name,
                this.props.station[this.props.name].outputStations[0],
                this.props.station[this.props.name].status.piecesDone
                )
        }

    };

    checkPiecesToDo = () => {

        let toDo = Object.keys(this.props.station[this.props.name].status.piecesToDo);
        let name = this.props.name;
        let statusObj = this.props.station[name].status;
        for(let i = 0; i < toDo.length; i++){
            if(statusObj.piecesToDo[toDo[i]][0]
                < statusObj.piecesToDo[toDo[i]][1]) return false;
        }

        return true;

    };

    makeTimeStamp = (nextProps) => {
        let preparing = nextProps.station[this.props.name].status.isPreparing;
        let working = nextProps.station[this.props.name].status.isWorking;

        let timeStamp = {
          start: null,
          style: null
        };

        let days = nextProps.gameInfo.day.number;
        let hours = nextProps.gameInfo.time.hour;
        let minutes = nextProps.gameInfo.time.minute;

        let sumOfMinutes = (days - 1) * 8 * 60 + hours * 60 + minutes;

        let ganttMinutes = Math.floor(sumOfMinutes / 60);
        let ganttSeconds = Math.floor(sumOfMinutes % 60);
        timeStamp.start = new Date(2000,1,1,1,ganttMinutes,ganttSeconds);


        if(preparing) timeStamp.style = "background-color: red";
        else if (working) timeStamp.style = "background-color: green";
        else timeStamp.style = "background-color: blue";

        if(
            this.props.station[this.props.name].status.isPreparing !== preparing ||
            this.props.station[this.props.name].status.isWorking !== working
        ) {
            this.props.onTimeStamp(timeStamp, this.props.name);
        }

    };




    render() {
        let statToRend = null;


        //Check if station is in Redux state.
        if(this.props.station.hasOwnProperty(this.props.name)){
            this.classes += ` ${this.props.station[this.props.name].resource}`;
            let lightIndicator = <Light
                buttonHandler={() => this.props.onPrepareStart(this.props.name)}
                activateYellow = {this.props.station[this.props.name].status.isPreparing && !this.props.gameInfo.isStarted}
                activateGreen = {this.props.station[this.props.name].status.isWorking && !(this.checkPiecesToDo()) }
                activateGreenFlash = {this.props.station[this.props.name].status.isWorking && (this.checkPiecesToDo()) }
                activateYellowFlash = {this.props.station[this.props.name].status.isPreparing && this.props.gameInfo.isStarted}
            />;



            statToRend = <Aux>
                <span className="todo">
                    To prepare: {Object.keys(this.props.station[this.props.name].status.piecesToDo).map((el) => {
                        return <li key={el}>{this.props.station[this.props.name].status.piecesToDo[el][0]}</li>
                })}

                </span>
                <div className="station-name"> Station {this.props.name} </div>
                {lightIndicator}
                <br/>
                <span className="done">
                    Done: {this.props.station[this.props.name].outputStations.length ? <Button
                    buttonHandler={() => this.transportButtonHandler()}
                    classes="btn btn-warning btn-sm"
                    text={this.props.station[this.props.name].status.piecesDone}/>
                    :
                    this.props.station[this.props.name].status.piecesDone}
                </span>
            </Aux>;
        }

        return (
            <div className="ws-wrapper">
                <div className={this.classes} id={this.props.name}>
                    {statToRend}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        activePurchasing: state.activePurchasing,
        transporting: state.showModal.transportingModal,
        station: state.stations,
        gameInfo: state.gameInfo
    }
};

const mapDispatachToProps = dispatch => {
    return {
        onActivateStation: (stationName) => dispatch({type: actionTypes.ACTIVATE_STATION, stationName: stationName}),
        onPrepareStart: (stationName) => dispatch({type: actionTypes.START_PREPARING, stationName: stationName}),
        onTimeTick: (stationName) => dispatch({type: actionTypes.TIME_TICK, stationName: stationName}),
        onPrepareTick: (stationName) => dispatch({type: actionTypes.PREPARE_TICK, stationName: stationName}),
        onPieceDone: (stationName) => dispatch({type: actionTypes.PIECE_DONE, stationName: stationName}),
        onPiecesTransport: (stationName, transportTo, amount) => dispatch({type: actionTypes.TRANSPORT_PIECES, stationName: stationName, transportTo: transportTo, amount: amount}),
        onPiecesSell: (stationName) => dispatch({type: actionTypes.SELL_PIECES, stationName: stationName}),
        onTransportModalShow: (transportingStation) => dispatch({type: actionTypes.SHOW_MODAL, transportingStation: transportingStation, modalToShow: "transportingModal"}),
        onTimeStamp: (timeStamp, stationName) => dispatch({type: actionTypes.TIME_STAMP, stationName: stationName, timeStamp: timeStamp})
    }
};

export default connect(mapStateToProps, mapDispatachToProps)(Station);
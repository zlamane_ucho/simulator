import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actionTypes from '../../../store/actions';
import './Modal.css';
import Backdrop from '../Backdrop/Backdrop';
import Aux from '../../../hoc/Auxiliary';

class Modal extends Component {

    shouldComponentUpdate(nextProps, nextState){
        //console.log((nextProps.show !== this.props.show || nextProps.children !== this.props.children));
        return nextProps.show !== this.props.show || nextProps.children !== this.props.children;
    }

    modalToRender = () => {

        if(this.props.children === null) return null;
        else {
            let classes = "Modal";
            if(this.props.classes) classes = this.props.classes;
            return <Aux>
                <Backdrop show={this.props.show}/>
                <div className={classes}
                     style={{
                         transform: this.props.show ? 'translateY(0)' : 'translateY(-100vh)',
                         opacity: this.props.show ? '1' : '0'
                     }}>
                    <div>{this.props.children}</div>
                </div>
            </Aux>
        }
    };

    render() {
        return this.modalToRender();
    }
}

const mapStateToProps = state => {
    return {
        activePurchasing: state.activePurchasing
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onPurchaseSubmit: (amount) => dispatch({type : actionTypes.ADD_PRODUCT_TO_STATION, amount: amount})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Modal);
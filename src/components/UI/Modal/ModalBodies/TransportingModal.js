import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actionTypes from '../../../../store/actions';
import '../Modal.css';
import Button from "../../Buttons/ControlButton";
import Aux from '../../../../hoc/Auxiliary';


class TransportingModal extends Component {

    constructor(props) {
        super(props);

        this.state = {
            value: 0,
        };

        this.inputHandler = this.inputHandler.bind(this);
    }

    buttonHandler = (element) => {
        this.props.onTransport(this.props.name, element, parseInt(this.state.value, 10));
        this.setState({value: 0});
        this.props.onModalHide();
    };


    buttonsToRender = () => {
        // console.log(this.props.stations[this.props.name]);
        //
        // return null;
        return this.props.stations[this.props.name].outputStations.map((element, i) => {
            let classesNames = `btn btn-primary btn-eq ${this.props.stations[element].resource}`;
            return <Button
                key = {i}
                classes={classesNames}
                buttonHandler={() => this.buttonHandler(element)}
                text={element}
            />
        })
    };

    inputHandler = e => {
        this.setState({value: e.target.value});
    };

    shouldComponentUpdate(nextProps, nextState){
        return nextProps.show !== this.props.show || nextProps.children !== this.props.children;
    }

    render() {
        return (
            <Aux>
                <span className="modal-span">Where do you want to transport pieces?</span><br/>
                {this.buttonsToRender()} <br/>
                <input id = "input"
                       type="number"
                       onChange={this.inputHandler}
                       className = "form-control form-eq"
                       placeholder="Amount"
                />
                <Button
                    classes = "btn btn-danger btn-mrg btn-db-eq"
                    buttonHandler={this.props.onModalHide}
                    text="CANCEL"/>
            </Aux>
        )

    }
}

const mapStateToProps = state => {
    return {
        activePurchasing: state.activePurchasing,
        stations: state.stations
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onModalHide: () => dispatch({type: actionTypes.HIDE_MODAL, modalToClose: "transportingModal"}),
        onTransport: (stationName, transportTo, amount) => dispatch({type : actionTypes.TRANSPORT_PIECES, stationName: stationName, transportTo: transportTo, amount: amount})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TransportingModal);
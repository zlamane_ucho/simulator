import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actionTypes from '../../../../store/actions';
import '../Modal.css';
import Button from "../../Buttons/ControlButton";
import Aux from '../../../../hoc/Auxiliary';


class EndOfWeekModal extends Component {


    shouldComponentUpdate(nextProps, nextState){
        return (nextProps.show !== this.props.show || nextProps.gameInfo.money !== this.props.gameInfo.money || nextProps.gameInfo.endOfWeek !== this.props.gameInfo.endOfWeek);
    }

    generateBoughtLi = () => {
        return Object.keys(this.props.gameInfo.costInfo.buying).map((el, i) => {
            if(this.props.gameInfo.endOfWeek.bought.hasOwnProperty(el)) {
                return <li key = {i} className="list-group-item d-flex justify-content-between align-items-center">
                    {el}
                    <span className="badge badge-primary badge-pill">{this.props.gameInfo.endOfWeek.bought[el]}</span>
                </li>
            } else {
                return <li key = {i} className="list-group-item d-flex justify-content-between align-items-center">
                    {el}
                    <span className="badge badge-primary badge-pill">0</span>
                </li>
            }
        })
    };

    generateSoldLi = () => {
        return Object.keys(this.props.gameInfo.costInfo.selling).map((el, i) => {
            if(this.props.gameInfo.endOfWeek.sold.hasOwnProperty(el)) {
                return <li key = {i} className="list-group-item d-flex justify-content-between align-items-center">
                    {el}
                    <span className="badge badge-primary badge-pill">{this.props.gameInfo.endOfWeek.sold[el]}</span>
                </li>
            } else {
                return <li key = {i} className="list-group-item d-flex justify-content-between align-items-center">
                    {el}
                    <span className="badge badge-primary badge-pill">0</span>
                </li>
            }
        })
    };

    buttonHandler = () => {
        this.props.onGameStatusCheck();
        this.props.onModalHide();
    };

    render() {
        return (
            <Aux>
                <ul className="list-group">
                    <li className="list-group-item d-flex justify-content-between align-items-center">
                        Start cash:
                        <span className="badge badge-primary badge-pill">{this.props.gameInfo.endOfWeek.startingMoney}</span>
                    </li>
                    <li className="list-group-item d-flex justify-content-between align-items-center">
                        Cash at end of the week:
                        <span className="badge badge-primary badge-pill">{this.props.gameInfo.money}</span>
                    </li>
                    <li className="list-group-item d-flex justify-content-between align-items-center">
                        Net cash flow:
                        <span className="badge badge-primary badge-pill">{this.props.gameInfo.money - this.props.gameInfo.endOfWeek.startingMoney}</span>
                    </li>
                    <li className="list-group-item d-flex justify-content-between align-items-center">
                        Bought
                        <ul className="list-group">
                            {this.generateBoughtLi()}
                        </ul>
                    </li>
                    <li className="list-group-item d-flex justify-content-between align-items-center">
                        Sold
                        <ul className="list-group">
                            {this.generateSoldLi()}
                        </ul>
                    </li>
                </ul>

                <Button
                    classes = "btn btn-dark"
                    buttonHandler={() => this.buttonHandler()}
                    text="HIDE"/>
            </Aux>
        )

    }
}

const mapStateToProps = state => {
    return {
        activePurchasing: state.activePurchasing,
        stations: state.stations,
        gameInfo: state.gameInfo
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onGameStatusCheck: () => dispatch({type: actionTypes.GAME_STATUS}),
        onModalHide: () => dispatch({type: actionTypes.HIDE_MODAL, modalToClose: "endOfWeekModal"}),
        onTransport: (stationName, transportTo) => dispatch({type : actionTypes.TRANSPORT_PIECES, stationName: stationName, transportTo: transportTo})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(EndOfWeekModal);
import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actionTypes from '../../../../store/actions';
import '../Modal.css';
import Aux from '../../../../hoc/Auxiliary';

class BuyingModal extends Component {

    constructor(props){
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    onSubmit(e) {
        e.preventDefault();
        let value = parseInt(e.target.querySelector("input").value, 10);
        e.target.querySelector("input").value = null;
        this.props.onPurchaseSubmit(value);
    }

    onCancel(e) {
        e.preventDefault();
        e.target.querySelector("input").value = null;
        this.props.modalClosed();
    }

    componentDidUpdate(){
        this.nameInput.focus();
    }

    shouldComponentUpdate(nextProps, nextState){
        return nextProps.show !== this.props.show || nextProps.children !== this.props.children;
    }

    render() {
        return (
            <Aux>
                <span className = "modal-span">How much do you want to buy?</span> <br/>
                    <form onSubmit={this.onSubmit} onReset = {this.onCancel}>
                        <input id = "input"
                               type="number"
                               className = "form-control form-eq"
                               ref={(input) => { this.nameInput = input; }}
                               placeholder="Amount"/>
                        <button type="submit" className="btn btn-success btn-eq">BUY</button>
                        <button type="reset" className="btn btn-danger btn-eq">CANCEL</button>
                    </form>
            </Aux>
        )
    }
}

const mapStateToProps = state => {
    return {
        activePurchasing: state.activePurchasing
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onPurchaseSubmit: (amount) => dispatch({type : actionTypes.ADD_PRODUCT_TO_STATION, amount: amount})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(BuyingModal);
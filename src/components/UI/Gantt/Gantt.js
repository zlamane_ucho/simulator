import Timeline from 'react-visjs-timeline'
import React from 'react';
import * as actionTypes from '../../../store/actions';
import {connect} from 'react-redux';
import './Gantt.css';

const options = {
    width: '100%',
    height: '50px',
    stack: false,
    start: new Date(2000,1,1,1,0,0),
    end: new Date(2000,1,1,1,40,0),
    timeAxis: {scale: 'year', step: 1},
    showMajorLabels: false,
    showMinorLabels: false,
    showCurrentTime: false,
    zoomMin: 1000000,
    moveable: true,
    type: 'background',
    format: {
        minorLabels: {
            minute: 'h:mma',
            hour: 'ha'
        }
    }
};

// const itemos = [
//     { start: new Date(2000,1,1,1,0), end: new Date(2000,1,1,1,5), style: "background-color: red"},
//     { start: new Date(2000,1,1,1,5), end: new Date(2000,1,1,1,10), style: "background-color: blue"},
//     { start: new Date(2000,1,1,1,10), end: new Date(2000,1,1,1,15), style: "background-color: green"}
// ];

class Gantt extends React.Component {


    shouldComponentUpdate(nextProps){

        return !(nextProps.gameInfo.time.minute === this.props.gameInfo.time.minute);

    }
    makeTimelineItems = () => {
        return Object.keys(this.props.ganttInfo).map((el) => {
            return this.props.ganttInfo[el].map((el, i, arr) => {
                let item = {
                    start: el.start,
                    end: null,
                    style: el.style,
                    content: ("A" + i)
                };
                if(arr.length > i + 1){
                    item.end = arr[i+1].start;
                } else {
                    item.end = this.calcEnd();
                }

                return item;

            })
        });
    };

    calcEnd = () => {
        let days = this.props.gameInfo.day.number;
        let hours = this.props.gameInfo.time.hour;
        let minutes = this.props.gameInfo.time.minute;

        let sumOfMinutes = (days - 1) * 8 * 60 + hours * 60 + minutes;

        let ganttMinutes = Math.floor(sumOfMinutes / 60);
        let ganttSeconds = Math.floor(sumOfMinutes % 60);
        return new Date(2000,1,1,1,ganttMinutes,ganttSeconds);
    };

    timelinesToRender = (items) => {
        return items.map((el, i) => {
            return <Timeline key={i} options = {options} items = {el}/>
        })
    };

    render() {
        let items = this.makeTimelineItems();
        return (
            <div>
                {this.timelinesToRender(items)}
                <button className="btn btn-danger btn-mrg" onClick={this.props.buttonHandler}>HIDE</button>
            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        gameInfo: state.gameInfo,
        ganttInfo: state.ganttInfo
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onGameStart: () => dispatch({type: actionTypes.START_GAME}),
        onSpeedUp: () => dispatch({type: actionTypes.SPEED_UP}),
        onSpeedDown: () => dispatch({type: actionTypes.SPEED_DOWN}),
        onGanttShow: () => dispatch({type: actionTypes.SHOW_MODAL, modalToShow: "ganttModal"}),
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Gantt);
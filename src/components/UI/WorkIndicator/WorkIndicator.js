import React from 'react';
import './WorkIndicator.css';

const workIndicator = (props) => {

    if (props.activateGreen) return <div onClick = {props.buttonHandler} className="Led-Green Light-Position"></div>;
    if (props.activateGreenFlash) return <div onClick = {props.buttonHandler} className="Led-Green-Flash Light-Position"></div>;
    if (props.activateYellow) return <div onClick = {props.buttonHandler} className="Led-Yellow Light-Position"></div>;
    if (props.activateYellowFlash) return <div onClick = {props.buttonHandler} className="Led-Green-Flash Light-Position"></div>;
    return <div onClick = {props.buttonHandler} className="Led-Red Light-Position"></div>;

};

export default workIndicator;
import React from "react";
import '../../../App.css';
import './ControlButton.css'

export default class Button extends React.Component {

    constructor() {
        super();
        this.state = {
            isClicked: false
        };
        this.click = this.click.bind(this);
    };

    click() {
        this.setState({
            isClicked: !this.state.isClicked
        });
        this.props.onGameStart();
    }

    render() {
        if (!this.props.isStarted) {
            return (
                <button onClick={this.click} className="btn btn-success btn-block btn-mrg-t">START</button>
            );
        } else {
            return (
                <button onClick={this.click} className="btn btn-danger btn-block btn-mrg-t">STOP</button>
            );
        }


    };
}

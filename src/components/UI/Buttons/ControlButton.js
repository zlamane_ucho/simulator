import React from 'react';
import './ControlButton.css';

const controlButton = (props) => {
        return (
            <button className={props.classes} onClick={props.buttonHandler}>
                {props.text}
            </button>
        )
};

export default controlButton;